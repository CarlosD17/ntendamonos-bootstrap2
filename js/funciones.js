
function calcularAltura(){
  altura = Math.max(document.documentElement.clientHeight, window.innerHeight);
  document.querySelector('.contenedor-principal').style.minHeight = altura+'px';
  document.querySelector('.contMenu').style.minHeight = altura+'px';
  document.querySelector('.menuDesplegable').style.minHeight = altura+'px';
}
window.addEventListener('orientationchange', calcularAltura, true);
window.addEventListener('resize', calcularAltura, true);
window.addEventListener('load', paginaActiva);


function paginaActiva(){
  setTimeout(function(){
    var url = window.location;
    var url = url.toString();
    var urls = url.split('/');
    var perfil = document.getElementById("perfilPag");
    var sugerencia = document.getElementById("sugerenciaPag");
    var revaluacion = document.getElementById("revaluacionPag");
    var relacion = document.getElementById("relacionPag");
    var unaPalabra = document.getElementById("unaPalabraPag");


    if (urls[4] == "perfil.html") {
        perfil.classList.add('paginaActiva');
    } else if (urls[4] == "aceptarSugerencia.html") {
        sugerencia.classList.add('paginaActiva');
    } else if (urls[4] == "aceptarReevaluacion.html") {
        revaluacion.classList.add('paginaActiva');
    } else if (urls[4] == "sugerirPalabra.html") {
        relacion.classList.add('paginaActiva');
    } else if (urls[4] == "agregarUnapalabra.html") {
        unaPalabra.classList.add('paginaActiva');
    }

  }, 50);

}
/**
* funcion desplegarNotificacion
* Cambia los estilos del div
* para mostrar y ocultar la notificacion
*/
function desplegarNotificacion(){
  mensaje.style.top="0";
  setTimeout(function(){
    mensaje.style.top="-30%";
  },3000);
}




function cambiar(){
  $("#paisRegistroR").css("padding-left","34.5%");
}
/**
* funcion palabra
* @param texto
* Obtiene el texto a mostrar en la notificacion
* lo agrega al elemento notificaciones
* manda llamar a la funcion desplegarNotificacion
*/
function palabra(texto){
    mensaje = document.getElementById("notificaciones");
    mensaje.innerHTML = "<h1>"+texto+"</h1>";
    desplegarNotificacion();
}

/**
* Function onchange Pais
* manda llamar funcion verificarDescripcion
*/
$(document).ready(function(){
  if($("#paisRegistroR").length){
      var con = "";
    $.ajax({
      url: 'Backend/obtenerPaises.php',
      type:'GET',
      crossDomain: true,
      success: function(data){
          data = JSON.parse(data);
          for(pais in data){
              con = con + "<option value='"+data[pais][0]+"'>"+data[pais][1]+"</option>";
          }
          $("#paisRegistroR").html(con);
      }
    });
  }
});
function validar_correo(correo){
  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  if (emailRegex.test(correo)) {
    return "correcto";
  } else {
    return "incorrecto";
  }
}

/**
* function registrar
* Mandar por ajax los datos para realizar el registro
* si existe algun error mostrarlos
*/
$(document).ready(function() {
 $("#registroBoton").click(function() {
        var name = $("#usuario").val();
        var password = $("#password").val();
        var veripassword = $("#verif_password").val();
        var email = $("#email").val();
        var pais = $("#paisRegistroR").val();
        validacion = validar_correo(email);
        if(validacion == "correcto"){
        self.palabra("Procesando..");
         $("#registroBoton").attr("disabled", true);
        if(name!=""&&password!=""&&veripassword!=""&&email!=""&&pais!=""){
            if(password == veripassword){
                $.ajax({
                  url: 'Backend/registrarUsuario.php',
                  type: 'POST',
                  data:{name: name, password:password, verif_password:veripassword, email:email, pais_id:pais},
                  success: function(data){
                    console.log(data);
                      data = data.trim();
                      if(data.trim() != "Usuario_Repetido"){
                          if(data.trim() != "Email_Repetido"){
                              if(data.trim() == "Registro_Exitoso"){
                                  self.palabra( " Registro correcto, confirma tu Email");
                                  setTimeout(function(){window.location = "index.html";},2000);
                              }000
                          }else{
                             self.palabra("Email ya registrado");
                             $("#registroBoton").attr("disabled", false);
                          }
                         }else{
                             self.palabra("Usuario ya registrado");
                             $("#registroBoton").attr("disabled", false);
                         }
                  }
                });
               }else{
                   self.palabra("Password no coinciden");
                   $("#registroBoton").attr("disabled", false);
               }
        }else{
            self.palabra("Completar campos");
            $("#registroBoton").attr("disabled", false);
        }
      }else{
        self.palabra("Correo invalido");
      }
	});
});

/**
* Function mostrarPass
* Muestra el cuadro para recuperar password
*/
$(document).ready(function(){
  $("#olvidarPassword").click(function(){
      $("#contSPass").css('visibility','visible');
      $("#contSPass").css('opacity','1');
      $("#contSin").css('visibility','visible');
  });
});

/**
* Function ocultarRecPas
* Oculta el cuadro para recuperar password
*/
function ocultarRecPas(){
  $("#contSPass").css('visibility','hidden');
  $("#contSPass").css('opacity','0');
  $("#contSin").css('visibility','hidden');
}

/**
* Function loginAcceso
* @param usuar
* @param passw
* Obtiene los datos de login y manda una petición ajax
* a la ruta login/autentificar para saber si el usuario existe
*/
$(function(){
    $("#login").click(function(){
        var usuario = $("#usuario").val();
        var password = $("#password").val();
        if(usuario != "" && password != ""){
            $.ajax({
                url: 'Backend/autentificarUsuario.php',
                type: 'POST',
                data:{usuario: usuario, password:password},
                success: function(data){
                if(data.trim() == "Acceso_Denegado"){
                    self.palabra("Usuario o contraseña incorrectos");
                  }else if(data.trim() == "No_Confirmacion"){
                    self.palabra("Confirmar cuenta");
                  }else if(data.trim() != "Error_Acceso"){
                    data = JSON.parse(data);
                      localStorage.setItem('usuario',data[2]);
                      localStorage.setItem('id_usuario',data[0]);
                      localStorage.setItem('fotoPerfil',data[3]);
                      localStorage.setItem('tipoUsuario',data[4]);
                      window.location = "principal.html";
                  }
                }
              });

        }else{
               self.palabra("Completar campos");
        }
    });
});

/**
 *  Funcion donde se realiza un login automatico
 *  Si no existe la varible en localStorage
 *  Espera a ingresar un user y password
 */
$(document).ready(function(){
  if($("#logAuto").length){
    if(localStorage.getItem('id_usuario')){
        var user = localStorage.getItem('id_usuario');
      $.ajax({
        url: 'Backend/registrarIngresoUsuario.php',
        type: 'POST',
        data:{usuario: user},
        success: function(data){
          if(data.trim() == "Acceso"){
            window.location = "principal.html";
          }
        }
      });
    }
  }
});


//__________________________Sección Perfil
/**
* Function cargarPerfil
* Obtiene el nombre del usuario y carga sus datos
* que se almacenaron en la BD
*/
$(document).ready(function(){
  if($("#perfil").length){
    var nombre = localStorage.getItem('usuario');
    $("#perfil").css('opacity','0');
    $.ajax({
      url: 'Backend/datosPerfil.php',
      type: 'POST',
      data:{nombre:nombre},
      success: function(data){
        data = JSON.parse(data);
        $("#correo").html(data[1]);
        $("#pais").html(data[4][0]);
        if(data[2] >= 0){
          $("#progreso").css('left',"0%");
        }else{
          $("#progreso").css('left', data[2]+"%");
        }
        setTimeout(function(){
          $("#perfil").attr('src','img/imgPerfil/'+data[3]);
          $("#perfil").css('opacity','1');
        },1000);
        $("#bandera").attr('src','img/banderas/'+data[4][1]);
      }
    });
  }
});

/**
* function cambiarImagen
* Activa el input type file
* Permite subir la nueva imagenIndex
*/
function cambiarImagen(){
  document.getElementById('cover').click();
  document.getElementById('guardarImg').style.display = "block";
  document.getElementById('cancelar').style.display = "block";
}

/**
* Function cancelarCambio
* Cancela el cambio de la imagen de perfil
* regresa la imagen de perfil que estab anteriormente
*/
function cancelarCambio(){
  $("#cover").val();
  $("#perfil").css('opacity','0');
  var nombre = localStorage.getItem('usuario');
  $.ajax({
    url: 'Backend/datosPerfil.php',
    type: 'POST',
    data:{nombre: nombre},
    success: function(data){
      data = JSON.parse(data);
      setTimeout(function(){$("#perfil").attr('src','img/imgPerfil/'+data[3]);
      $("#perfil").css('opacity','1');
    },1000);
      $("#guardarImg").css('display','none');
      $("#cancelar").css('display','none');
    }
  });
}

$(function(){
    $("#cover").change(function(){
        var val = $("#cover").val();
        var file_type = val.substr(val.lastIndexOf('.')).toLowerCase();
        if (file_type  === '.jpg') {
            $("#perfil").css('opacity','0');
            self.change_image(this, 'perfil');
            sessionStorage.setItem("ultimaFoto",$("#perfil").attr("src"));
        }  else{
            $("#cover").val("");
        self.palabra("Subir archivo .jpg");
        }
    });
});

/**
* Function cambiar Imagen Front
* Cambia la imagen del usuario solamente en el front
*/

function change_image(input, tipo){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          if (tipo == "perfil") {
            setTimeout(function(){$('#perfil').attr('src', e.target.result);
              $("#perfil").css('opacity','1');
            },1000);
          } else{
            setTimeout(function(){$('#imgPerM').attr('src', e.target.result);
              $("#imgPerM").css('opacity','1');
            },1000);
          }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/**
* Function guardarImagen
* Realiza el cambio en la base de datos
* almacena la nueva imagen de perfil en la carpeta img/imgPerfil
*/
$(document).ready(function(){
    $("#guardarImg").click(function(){
      var file = $("#cover").val();
      var formData = new FormData($('#prueba')[0]);
      var nombre = localStorage.getItem('usuario');
      formData.append("nombre", nombre);
      if(file != ""){
        self.palabra("Guardando cambios");
        $.ajax({
          url: 'Backend/actualizarFoto.php',
          type: 'POST',
          processData : false,
          contentType: false,
          data: formData,
          success: function(data){
            if(data.trim() == "Exitoso"){
              $("#cancelar").css('display','none');
              $("#guardarImg").css('display','none');
              localStorage.setItem('fotoPerfil',nombre+".jpg");
              $("#imgPerM").attr('src','img/imgPerfil/'+nombre+".jpg");
              $("#imgPerM").css('opacity','0');
              input = document.getElementById('cover');
              self.change_image(input, 'perfil2');
            }
          }
        });
      }else{
          self.palabra("Ingresar una imagen");
      }
  });
});
/**
* Function recuperarPass
* Comprueba si existe el usuario
* para enviar el email
*/
function recuperarPass(){
  var user = $("#UserPass").val();
  if(user != ""){
    $.ajax({
      url: 'Backend/recuperarPass.php',
      type:'GET',
      data: {name:user},
      success: function(data){
        if(data.trim() == "Acceso Denegado"){
          self.palabra("No existe el usuario");
        }else if(data.trim() == "Correcto"){
          self.ocultarRecPas();
          $("#UserPass").val("");
          self.palabra("Se te ha enviado un email");
        }
      }
    });
  }else{
    self.palabra("Ingrese un nombre de usuario");
  }
}

/**
* confirmarPassword
* Obtiene los datos de las nuevas password
* Enviar ajax a la ruta /login/reestablecerPassword
* En el controlador se hacen las autentificaciones necesarias
* si existe un error estos son mostrados por el ajax
* si no redirecciona a /login con un mensaje mandado desde el controlador
*/
$(document).ready(function(){
  $("#confirmarPassword").click(function(){
    var password = $("#password").val();
    var veripassword = $("#verifpassword").val();
    var user = $_GET("user");
    if (password == "" && veripassword =="") {
      self.palabra("Completar campos");
    } else if (password == veripassword) {
      $.ajax({
        url: 'Backend/cambiarPassword.php',
        type: 'POST',
        data:{password:password, verif_password:veripassword, user:user},
        success: function(data){
          if(data.trim() == "Correcto"){
            self.palabra("Contraseña cambiada");
            setTimeout(function(){
              window.location="index.html";
            },3000);
          }
        }
      });
    } else{
      self.palabra("Contraseñas no coinciden");
    }
  });
});

function $_GET(param) {
  url = document.URL;
  url = String(url.match(/\?+.+/));
  url = url.replace("?", "");
  url = url.split("&");
  x = 0;
  while (x < url.length)
  {
  p = url[x].split("=");
  if (p[0] == param)
  {
  return decodeURIComponent(p[1]);
  }
  x++;
  }
}

function notiIndex(){
  var notificacion = $_GET("not");
  if (notificacion == "correcto") {
      self.palabra("Autentificado correctamente");
  } else if (notificacion == "error") {
      self.palabra("Codigo no valido");
  }

}

$(function(){
    $("#cancelarPassword").click(function(){
        window.location = "index.html";
    });
});

function recPass(){
  data = "<h3 class='title text-center'>Recuperar contraseña</h3>"+
    "<form method='post' onsubmit='return false'>"+
      "<input type='hidden' name='_token' value='{{ csrf_token() }}' id='token'>"+
      "<div class='form-group'>"+
        "<label>Usuario</label>"+
        "<input type='text' class='form-control' placeholder='Usuario' name='usuario' id='UserPass' >"+
      "</div>"+
      "<br>"+
      "<div class='col-xs-12 text-center'>"+
        "<button type='submit' class='btn btn-primary hover' id='recuperarp' onclick='recuperarPass()'>Recuperar contraseña</button><br>"+
        "<a class='invitado text-center' onclick='backlogin()'>Regresar al Inicio de sesión</a><br>"+
    " </div>"+
    "</form>";
  $("#datos_index").html(data);
}


function backlogin(){
 data = "<h3 class='title text-center'>Iniciar Sesión</h3>"+

  "<form method='post' onsubmit='return false'>"+
      "<input type='hidden' name='_token' value='{{ csrf_token() }}' id='token'>"+
    "<div class='form-group'>"+
      "<label >Usuario</label>"+
      "<input type='text' class='form-control' onfocus='this.placeholder=''' placeholder='Usuario' name='usuario' id='usuario' onblur='this.placeholder='Usuario''>"+
    "</div>"+
    "<div class='form-group'>"+
      "<label>Contraseña</label>"+
      "<input type='password' class='form-control' placeholder='Contraseña' type='password' onfocus='this.placeholder=''' name='password' id='password' onblur='this.placeholder='Password''>"+
    "</div>"+
    "<br>"+
    "<div class='col-xs-12 text-center'>"+
      "<button type='submit' class='btn btn-primary hover' id='login'>Ingresar</button><br>"+
      "<a class='invitado text-center' onclick='recPass()'>¿Has olvidado tu contraseña?</a><br>"+
      "<a class='invitado text-center' onclick='invitado()'>Iniciar sesión como invitado</a>"+
    "</div>"+
  "</form>";
  $("#datos_index").html(data);
}
