
<?php
require 'PHPMailer/PHPMailerAutoload.php';

function enviar_email($tipo, $correo, $clave, $usuario){
  try {
  // admin@ifilac.net
  // Cdesarrollo2017!
  $mail = new PHPMailer(true);
  $mail->IsSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->SMTPAuth = true;
  $mail->Port = 587;
  $mail->SMTPSecure = "tls";
  $mail->Host = 'smtp.gmail.com';
  $mail->Username  = "glosariohidrologicolat@gmail.com";
  $mail->Password = "GHLCDesarrollo2018";
  $mail->From = $correo;
  $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

  $mail->FromName = "Glosario Hidrológico Latinoamericano";
  $mail->AddAddress($correo);
    $mail->AddEmbeddedImage("../img/icono.png", "logo");
  $body = "";
  if ($tipo == "autentificar") {

    $mail->Subject = "Confirmacion De Cuenta";
    $body  = "<div style='width:50%; '>
                  <div style='background: rgba(255, 255, 255, 255);'>
                    <img style='margin-left:20%; margin-top:20px; width:50%;' src='cid:logo' alt='logo'>
                  </div>
                  <div style='' >
                    <h1 style='font-family: sans-serif; padding-top: 15px; padding-bottom: 15px; color:rgb(0,0,0); font-size:20px;'><b>Glosario hidrológico latinoamericano</h1>
                  </div>
                  <div style='flex-direction:column; padding-left:20px; padding-right:20px;'>
                    <h3 style='font-family: sans-serif; color:rgb(0,0,0); font-weight:bold !important; border-bottom: solid 1px rgba(0,0,0,.1); padding-bottom:15px;'>Hola, {$usuario}:</h3>
                    <h5 style='font-family: sans-serif; color:rgb(0,0,0); font-weight:lighter !important;'>GRACIAS POR REGISTRARTE</h5>
                    <h5 style='font-family: sans-serif; color:rgb(0,0,0); font-weight:lighter !important;'>Para finalizar tu registro necesitamos que confirmes tu correo dando click en el siguiente enlace</h5>
                    <h4><a style='text-decoration:none;' href='http://localhost:9999/doc/ntendamonos-bootstrap2/Backend/comprobarCodigo.php?codigo=$clave&tipo=$tipo&user=$usuario'>Confirmar Cuenta</a></h4>
                    <h6 style='font-family: sans-serif; color:rgb(0,0,0); font-weight:lighter !important;'>Si no has solicitado unirte a la comunidad, por favor ignora este email.</h6>
                  </div>
                  <div style='background: rgb(0,96,180); padding-right:15px; margin-top:40px;'>
                      <h5 style='font-family: sans-serif; padding-top: 10px; padding-bottom: 10px; color:rgb(255,255,255);text-align: right;'>Equipo de cuentas de Glosario hidrológico latinoamericano</h5>
                  </div>
      </div>";
  } elseif ($tipo == "password") {

    $mail->Subject = "Recuperación De Contraseña";
    $body  = "<div class='contenedor'><div class='headerEmail'><img src='' alt='logo'><h1 style='padding-top: 35px;'>Glosario hidrológico latinoamericano</h1></div><div class='cuerpoMensaje'><h1>Hola, $usuario:</h1><h3>Para recuperar tu contraseña, es necesario que vayas a la siguiente dirección: </h3><br><a href='http://148.220.209.253:9999/ntendamonos-bootstrap2/Backend/comprobarCodigo.php?codigo=$clave&tipo=$tipo&user=$usuario'>Recuperar Password</a><br><h3>Si no has solicitado cambiar tu contraseña, por favor ignora este email y tu contraseña seguirá sin cambiar.</h3><h3 id='atte'>Atentamente,<br>El equipo de cuentas de Ntendamonos</h3></div></div>";
  }
  $mail->Body = $body;
  $mail->IsHTML(true);

  if ($mail->Send()) {
    return "Correo Enviado";
  }
  } catch(Exception $e) {
      echo 'Excepción capturada:';
  }
}

function generateRandomString($length = 10) {
  return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}
// $tipo = "autentificar";
// $correo = "alemoralesb11@gmail.com";
// $clave = "HelloWorld";
// $nombre = "Estefania";
// enviar_email($tipo,$correo,$clave,$nombre);

?>
