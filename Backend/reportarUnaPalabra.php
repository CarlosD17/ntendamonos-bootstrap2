<?php
    require("conex.php");
    $con = conexion();
    $id = $_GET["id"];
    $usuario = $_GET["usuario"];
    $descripcion = $_GET["descripcion"];
    // $id = 15;
    // $usuario = 2;
    $qryid = "SELECT * FROM palabras WHERE id = {$id}";
    $idres= $con->query($qryid);
    $aux = "correcta";
    while($datosid = $idres->fetch_row()){
        if ($datosid[0] != null) {
              $puntos = $datosid[9] + 1;
              if($puntos >= 3){
                  disminuirPuntos($usuario, $con);
                  $aux = "reportada";
              }
              $qryIn = "INSERT INTO reporte (Descripcion, palabras_id, users_id,tipo, estatus) VALUES ('{$descripcion}', '{$id}', '{$usuario}', 'palabra', '1' )";
              $con->query($qryIn);
              $qryUp = "UPDATE palabras SET reportada = {$puntos}, estatus = '{$aux}' WHERE id = {$id}";
              if($con->query($qryUp)){
                if ($puntos >= 3) {
                  echo "correcto+3";
                } else{
                  echo "correcto";
                }
              }
        }
    }

    /**
    * Function disminuirPuntos
    * Funcion que disminuye el puntaje de los usuarios
    * Si el puntaje es menor a 0 pasa a ser usuario normal
    * @param $idUsuario
    */
    function disminuirPuntos($idUsuario, $con){
        $qryUs = "SELECT * FROM users WHERE id = {$idUsuario}";
        $resUs = $con->query($qryUs);
        while($datosUs = $resUs->fetch_row()){
            $puntosUs = $datosUs[8]-5;
            $auxUs = "";
            if($puntosUs < 0){
                $auxUs = "normal";
            }else{
                $auxUs = "moderador";
            }
            if ($puntosUs <= -100) {
              $puntosUs = -100;
            }
            $qryUsUp = "UPDATE users SET tipo = '{$auxUs}', puntos = {$puntosUs} WHERE id = {$idUsuario}";
            $con->query($qryUsUp);
        }
    }
?>
