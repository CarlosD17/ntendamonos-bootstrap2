<?php
    require("conex.php");
    $con = conexion();
    $array = [];
    $qry = "SELECT palabra FROM palabras WHERE estatus = 'correcta' ORDER BY palabra";
    $res = $con->query($qry);

    while($datos = $res->fetch_row()){
      $array[] = $datos[0];
    }
    echo json_encode($array, JSON_UNESCAPED_UNICODE);
?>
