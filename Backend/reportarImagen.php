<?php
  require("conex.php");
  $con = conexion();

  $idImagen = $_GET["imagen_id"];
  // $idImagen = 15;

  $query_select = "SELECT * FROM imagen WHERE idImagen = {$idImagen}";
  $resultado  = $con->query($query_select)->fetch_row();

  $num_reportes = $resultado[6]+1;
  $filename = "../img/imgPalabras/{$resultado[3]}/{$resultado[5]}";
  $carpeta = @scandir("../img/imgPalabras/{$resultado[3]}");
  if ($num_reportes < 3) {
    $query = "UPDATE imagen SET reporte = {$num_reportes} WHERE idImagen = {$idImagen}";
    $con->query($query);
    echo "imagen reportada";

  } else {
    $query = "UPDATE imagen SET reporte = {$num_reportes} WHERE idImagen = {$idImagen}";
    $con->query($query);
    if (file_exists($filename)) {
      unlink($filename);
      if (count($carpeta) == 3) {
        rmdir("../img/imgPalabras/{$resultado[3]}");
      }
      echo "imagen eliminada";
    } else {
      echo "la imagen ya fue eliminada";
    }
  }

?>
