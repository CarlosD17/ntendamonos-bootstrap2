<?php
require("conex.php");
    $con = conexion();
    $id = $_GET["id"];
    $usuario = $_GET["usuario"];
    $id_palabraP = $_GET["idPalabra"];
    $descripcion = $_GET["descripcion"];
    $tipo = "relacion";
    $qryid = "SELECT * FROM verificar_palabras WHERE id_palabra1 = {$id_palabraP} AND id_palabra2 = {$id}";
    // $qryid = "SELECT * FROM verificar_palabras WHERE (id_palabra1 = {$id_palabraP} AND id_palabra2 = {$id}) OR (id_palabra1 ={$id} AND id_palabra2 ={$id_palabraP})";
    $idres= $con->query($qryid);
    while($datos = $idres->fetch_row()){
        if ($datos[0] != null) {
            $puntos = $datos[5] + 1;
            $aux = "aprobada";
            if($puntos >= 3){
                disminuirPuntos($datos[1], $con);
                disminuirPuntos($datos[4], $con);
                $aux = "rechazada";
            }
            $qryIn = "INSERT INTO reporte (Descripcion, palabras_id, users_id, tipo, estatus) VALUES ('{$descripcion}', '{$datos[0]}', '{$usuario}','{$tipo}', '1')";
            $con->query($qryIn);
            $qryUp = "UPDATE verificar_palabras SET reportada = {$puntos}, estatus = '{$aux}' WHERE id = {$datos[0]}";
            if($con->query($qryUp)){
                if ($puntos >= 3) {
                echo "correcto+3";
                break;
              } else{
                echo "correcto";
                break;
              }

            }
        }
    }

    /**
    * Function disminuirPuntos
    * Funcion que disminuye el puntaje de los usuarios
    * Si el puntaje es menor a 0 pasa a ser usuario normal
    * @param $idUsuario
    */
    function disminuirPuntos($idUsuario, $con){
        $qryUs = "SELECT * FROM users WHERE id = {$idUsuario}";
        $resUs = $con->query($qryUs);
        while($datosUs = $resUs->fetch_row()){
            $puntosUs = $datosUs[8]-5;
            $auxUs = "";
            if($puntosUs < 0){
                $auxUs = "normal";
            }else{
                $auxUs = "moderador";
            }
            if ($puntosUs <= -100) {
              $puntosUs = -100;
            }
            $qryUsUp = "UPDATE users SET tipo = '{$auxUs}', puntos = {$puntosUs} WHERE id = {$idUsuario}";
            $con->query($qryUsUp);
        }
    }
?>
