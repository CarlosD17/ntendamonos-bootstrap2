<?php
    require("conex.php");
    $con = conexion();
    $sugerida = $_GET["palabraSugerida"];
    $paisSugerida = $_GET["paisSugerida"];
    $definicion1 = $_GET["definicion1"];
    $definicion_unesco = $_GET["definicion_unesco"];
    $definicion_ifilac = $_GET["definicion_ifilac"];
    $query = "SELECT nombrePais FROM pais WHERE id = {$paisSugerida}";
    $id_relacion = $con->query($query)->fetch_array();

    $pais_texto = $id_relacion[0];
    $idPalabraR = "";
    $res = operacionPalabra($sugerida,$paisSugerida, $con,$definicion1,$definicion_ifilac, $definicion_unesco);
    if($res == "Exitoso"){
      echo "Exitoso";
    }else{
      echo "Duplicado";
    }

/**
     *  function operacionPalabra
     *  Insertar palabras en Palabra
     *  @param $palabra
     *  @param $idPais
     *  @return $idPalabra1
     */
    function operacionPalabra($palabra, $idPais, $con, $definicion1 ,$definicion_ifilac, $definicion_unesco){
        $idPalabra1= "";
        $qry = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
        $res = $con->query($qry);
        if($res->num_rows > 0){
            $res = $res->fetch_array();
            return "duplicado";
        }else{
            $date = date('Y-m-d H:i:s');
            $qry2 = "INSERT INTO palabras (palabra, id_pais, estatus, created_at, updated_at, Definicion, glosario_ifilac, Glosario_unesco ) VALUES ('{$palabra}', {$idPais}, 'correcta', '{$date}', '{$date}', '{$definicion1}', '{$definicion_ifilac}', '{$definicion_unesco}')";
            if($con->query($qry2)){
                $qry3 = "SELECT id FROM palabras WHERE palabra LIKE '{$palabra}' AND id_pais = {$idPais}";
                $res3 = $con->query($qry3);
                $res3 = $res3->fetch_row();
                $idPalabra1 = $res3[0];
            }
            return "Exitoso";
        }
    }
 ?>
